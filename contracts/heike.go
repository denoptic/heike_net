// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package contracts

import (
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = abi.U256
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// HeikeRetainerABI is the input ABI used to generate the binding from.
const HeikeRetainerABI = "[{\"constant\":false,\"inputs\":[],\"name\":\"pay\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"inputText\",\"type\":\"string\"}],\"name\":\"newProject\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"message\",\"outputs\":[{\"name\":\"\",\"type\":\"string\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"}]"

// HeikeRetainerBin is the compiled bytecode used for deploying new contracts.
const HeikeRetainerBin = `0x608060405234801561001057600080fd5b506102c3806100206000396000f3006080604052600436106100565763ffffffff7c01000000000000000000000000000000000000000000000000000000006000350416631b9265b8811461005b578063e1cac57b14610072578063e21f37ce146100cb575b600080fd5b34801561006757600080fd5b50610070610155565b005b34801561007e57600080fd5b506040805160206004803580820135601f81018490048402850184019095528484526100709436949293602493928401919081908401838280828437509497506101579650505050505050565b3480156100d757600080fd5b506100e061016e565b6040805160208082528351818301528351919283929083019185019080838360005b8381101561011a578181015183820152602001610102565b50505050905090810190601f1680156101475780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b565b805161016a9060009060208401906101fc565b5050565b6000805460408051602060026001851615610100026000190190941693909304601f810184900484028201840190925281815292918301828280156101f45780601f106101c9576101008083540402835291602001916101f4565b820191906000526020600020905b8154815290600101906020018083116101d757829003601f168201915b505050505081565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061023d57805160ff191683800117855561026a565b8280016001018555821561026a579182015b8281111561026a57825182559160200191906001019061024f565b5061027692915061027a565b5090565b61029491905b808211156102765760008155600101610280565b905600a165627a7a7230582041b2bd8ff3b2fb982418bcad5e60c40dc9d9b23b8960751d3d75b5c68b32e2b90029`

// DeployHeikeRetainer deploys a new Ethereum contract, binding an instance of HeikeRetainer to it.
func DeployHeikeRetainer(auth *bind.TransactOpts, backend bind.ContractBackend) (common.Address, *types.Transaction, *HeikeRetainer, error) {
	parsed, err := abi.JSON(strings.NewReader(HeikeRetainerABI))
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	address, tx, contract, err := bind.DeployContract(auth, parsed, common.FromHex(HeikeRetainerBin), backend)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &HeikeRetainer{HeikeRetainerCaller: HeikeRetainerCaller{contract: contract}, HeikeRetainerTransactor: HeikeRetainerTransactor{contract: contract}, HeikeRetainerFilterer: HeikeRetainerFilterer{contract: contract}}, nil
}

// HeikeRetainer is an auto generated Go binding around an Ethereum contract.
type HeikeRetainer struct {
	HeikeRetainerCaller     // Read-only binding to the contract
	HeikeRetainerTransactor // Write-only binding to the contract
	HeikeRetainerFilterer   // Log filterer for contract events
}

// HeikeRetainerCaller is an auto generated read-only Go binding around an Ethereum contract.
type HeikeRetainerCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// HeikeRetainerTransactor is an auto generated write-only Go binding around an Ethereum contract.
type HeikeRetainerTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// HeikeRetainerFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type HeikeRetainerFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// HeikeRetainerSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type HeikeRetainerSession struct {
	Contract     *HeikeRetainer    // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// HeikeRetainerCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type HeikeRetainerCallerSession struct {
	Contract *HeikeRetainerCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts        // Call options to use throughout this session
}

// HeikeRetainerTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type HeikeRetainerTransactorSession struct {
	Contract     *HeikeRetainerTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts        // Transaction auth options to use throughout this session
}

// HeikeRetainerRaw is an auto generated low-level Go binding around an Ethereum contract.
type HeikeRetainerRaw struct {
	Contract *HeikeRetainer // Generic contract binding to access the raw methods on
}

// HeikeRetainerCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type HeikeRetainerCallerRaw struct {
	Contract *HeikeRetainerCaller // Generic read-only contract binding to access the raw methods on
}

// HeikeRetainerTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type HeikeRetainerTransactorRaw struct {
	Contract *HeikeRetainerTransactor // Generic write-only contract binding to access the raw methods on
}

// NewHeikeRetainer creates a new instance of HeikeRetainer, bound to a specific deployed contract.
func NewHeikeRetainer(address common.Address, backend bind.ContractBackend) (*HeikeRetainer, error) {
	contract, err := bindHeikeRetainer(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &HeikeRetainer{HeikeRetainerCaller: HeikeRetainerCaller{contract: contract}, HeikeRetainerTransactor: HeikeRetainerTransactor{contract: contract}, HeikeRetainerFilterer: HeikeRetainerFilterer{contract: contract}}, nil
}

// NewHeikeRetainerCaller creates a new read-only instance of HeikeRetainer, bound to a specific deployed contract.
func NewHeikeRetainerCaller(address common.Address, caller bind.ContractCaller) (*HeikeRetainerCaller, error) {
	contract, err := bindHeikeRetainer(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &HeikeRetainerCaller{contract: contract}, nil
}

// NewHeikeRetainerTransactor creates a new write-only instance of HeikeRetainer, bound to a specific deployed contract.
func NewHeikeRetainerTransactor(address common.Address, transactor bind.ContractTransactor) (*HeikeRetainerTransactor, error) {
	contract, err := bindHeikeRetainer(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &HeikeRetainerTransactor{contract: contract}, nil
}

// NewHeikeRetainerFilterer creates a new log filterer instance of HeikeRetainer, bound to a specific deployed contract.
func NewHeikeRetainerFilterer(address common.Address, filterer bind.ContractFilterer) (*HeikeRetainerFilterer, error) {
	contract, err := bindHeikeRetainer(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &HeikeRetainerFilterer{contract: contract}, nil
}

// bindHeikeRetainer binds a generic wrapper to an already deployed contract.
func bindHeikeRetainer(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(HeikeRetainerABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_HeikeRetainer *HeikeRetainerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _HeikeRetainer.Contract.HeikeRetainerCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_HeikeRetainer *HeikeRetainerRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.HeikeRetainerTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_HeikeRetainer *HeikeRetainerRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.HeikeRetainerTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_HeikeRetainer *HeikeRetainerCallerRaw) Call(opts *bind.CallOpts, result interface{}, method string, params ...interface{}) error {
	return _HeikeRetainer.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_HeikeRetainer *HeikeRetainerTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_HeikeRetainer *HeikeRetainerTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.contract.Transact(opts, method, params...)
}

// Message is a free data retrieval call binding the contract method 0xe21f37ce.
//
// Solidity: function message() constant returns(string)
func (_HeikeRetainer *HeikeRetainerCaller) Message(opts *bind.CallOpts) (string, error) {
	var (
		ret0 = new(string)
	)
	out := ret0
	err := _HeikeRetainer.contract.Call(opts, out, "message")
	return *ret0, err
}

// Message is a free data retrieval call binding the contract method 0xe21f37ce.
//
// Solidity: function message() constant returns(string)
func (_HeikeRetainer *HeikeRetainerSession) Message() (string, error) {
	return _HeikeRetainer.Contract.Message(&_HeikeRetainer.CallOpts)
}

// Message is a free data retrieval call binding the contract method 0xe21f37ce.
//
// Solidity: function message() constant returns(string)
func (_HeikeRetainer *HeikeRetainerCallerSession) Message() (string, error) {
	return _HeikeRetainer.Contract.Message(&_HeikeRetainer.CallOpts)
}

// NewProject is a paid mutator transaction binding the contract method 0xe1cac57b.
//
// Solidity: function newProject(inputText string) returns()
func (_HeikeRetainer *HeikeRetainerTransactor) NewProject(opts *bind.TransactOpts, inputText string) (*types.Transaction, error) {
	return _HeikeRetainer.contract.Transact(opts, "newProject", inputText)
}

// NewProject is a paid mutator transaction binding the contract method 0xe1cac57b.
//
// Solidity: function newProject(inputText string) returns()
func (_HeikeRetainer *HeikeRetainerSession) NewProject(inputText string) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.NewProject(&_HeikeRetainer.TransactOpts, inputText)
}

// NewProject is a paid mutator transaction binding the contract method 0xe1cac57b.
//
// Solidity: function newProject(inputText string) returns()
func (_HeikeRetainer *HeikeRetainerTransactorSession) NewProject(inputText string) (*types.Transaction, error) {
	return _HeikeRetainer.Contract.NewProject(&_HeikeRetainer.TransactOpts, inputText)
}

// Pay is a paid mutator transaction binding the contract method 0x1b9265b8.
//
// Solidity: function pay() returns()
func (_HeikeRetainer *HeikeRetainerTransactor) Pay(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _HeikeRetainer.contract.Transact(opts, "pay")
}

// Pay is a paid mutator transaction binding the contract method 0x1b9265b8.
//
// Solidity: function pay() returns()
func (_HeikeRetainer *HeikeRetainerSession) Pay() (*types.Transaction, error) {
	return _HeikeRetainer.Contract.Pay(&_HeikeRetainer.TransactOpts)
}

// Pay is a paid mutator transaction binding the contract method 0x1b9265b8.
//
// Solidity: function pay() returns()
func (_HeikeRetainer *HeikeRetainerTransactorSession) Pay() (*types.Transaction, error) {
	return _HeikeRetainer.Contract.Pay(&_HeikeRetainer.TransactOpts)
}
