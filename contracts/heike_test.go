package contracts

import (
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/accounts/abi/bind/backends"
	"github.com/ethereum/go-ethereum/core"
	"github.com/ethereum/go-ethereum/crypto"
	"math/big"
	"testing"
)

func TestDeployHeikeRetainer(t *testing.T) {

	//Setup simulated block chain
	key, _ := crypto.GenerateKey()
	auth := bind.NewKeyedTransactor(key)
	alloc := make(core.GenesisAlloc)
	alloc[auth.From] = core.GenesisAccount{Balance: big.NewInt(1000000000)}
	blockchain := backends.NewSimulatedBackend(alloc, 0)

	//Deploy contract
	address, _, _, err := DeployHeikeRetainer(auth, blockchain)

	// commit all pending transactions
	blockchain.Commit()

	if err != nil {
		t.Fatalf("Failed to deploy the HeikeRetainer contract: %v", err)
	}

	if len(address.Bytes()) == 0 {
		t.Error("Expected a valid deployment address. Received empty address byte array instead")
	}
}

func TestNewProject(t *testing.T) {

	//Setup simulated block chain
	key, _ := crypto.GenerateKey()
	auth := bind.NewKeyedTransactor(key)
	alloc := make(core.GenesisAlloc)
	alloc[auth.From] = core.GenesisAccount{Balance: big.NewInt(1000000000)}
	blockchain := backends.NewSimulatedBackend(alloc, 0)

	//Deploy contract
	_, _, contract, _ := DeployHeikeRetainer(auth, blockchain)

	// commit all pending transactions
	blockchain.Commit()

	test_string := "okla"

	contract.NewProject(&bind.TransactOpts{
		From:   auth.From,
		Signer: auth.Signer,
		Value:  nil,
	}, test_string)

	// commit all pending transactions
	blockchain.Commit()

	if result, _ := contract.Message(nil); result != test_string {
		t.Errorf("Expected message to be: %s. Got: %s", test_string, result)
	}
}

// func TestGetProjects(t *testing.T) {
//
// 	//Setup simulated block chain
// 	key, _ := crypto.GenerateKey()
// 	auth := bind.NewKeyedTransactor(key)
// 	alloc := make(core.GenesisAlloc)
// 	alloc[auth.From] = core.GenesisAccount{Balance: big.NewInt(1000000000)}
// 	blockchain := backends.NewSimulatedBackend(alloc, 0)
//
// 	//Deploy contract
// 	_, _, contract, _ := DeployHeikeRetainer(auth, blockchain)
//
// 	// commit all pending transactions
// 	blockchain.Commit()
//
// 	test_string := "okla"
//
// 	contract.NewProject(&bind.TransactOpts{
// 		From:   auth.From,
// 		Signer: auth.Signer,
// 		Value:  nil,
// 	}, test_string)
//
// 	// commit all pending transactions
// 	blockchain.Commit()
//
// 	result, _ := contract.GetProjects(&bind.TransactOpts{
// 		From:   auth.From,
// 		Signer: auth.Signer,
// 		Value:  nil,
// 	})
//
// 	// commit all pending transactions
// 	blockchain.Commit()
//
// 	if result != nil {
// 		fmt.Println(result.hash)
// 		t.Errorf("result is not nil")
// 	}
//
// 	// type Transaction struct {
// 	// 	data txdata
// 	// 	// caches
// 	// 	hash atomic.Value
// 	// 	size atomic.Value
// 	// 	from atomic.Value
// 	// }
// 	//
// 	// type txdata struct {
// 	// 	AccountNonce uint64          `json:"nonce"    gencodec:"required"`
// 	// 	Price        *big.Int        `json:"gasPrice" gencodec:"required"`
// 	// 	GasLimit     uint64          `json:"gas"      gencodec:"required"`
// 	// 	Recipient    *common.Address `json:"to"       rlp:"nil"` // nil means contract creation
// 	// 	Amount       *big.Int        `json:"value"    gencodec:"required"`
// 	// 	Payload      []byte          `json:"input"    gencodec:"required"`
//
// 	// fmt.Println("--------------------------")
// 	// fmt.Println(result)
// 	// fmt.Println("--------------------------")
//
// 	// str, err := s.helloworld.Say(nil)
// 	// s.Equal("hello etherworld", str)
// 	// s.Nil(err)
//
// 	// if result, _ := contract.GetProjects(nil); result != test_string {
// 	// 	t.Errorf("Expected message to be: %s. Got: %s", test_string, result)
// 	// }
// }
