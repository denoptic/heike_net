package main

import (
	"errors"
	"log"
	"net/http"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	// "github.com/dgrijalva/jwt-go"
	// "golang.org/x/crypto/bcrypt"

	"gitlab.com/denoptic/heike_net/models"
)

const EXP = 72

func home(c echo.Context) error {
	// db, err := getDB(c)
	// if err != nil {
	// 	return err
	// }
	//
	// var user models.User
	// if db.First(&user).RecordNotFound() {
	// 	return c.String(http.StatusOK, "no users")
	// } else {
	// 	return c.JSON(http.StatusOK, user)
	// }
	return c.String(http.StatusOK, "welcome to heike-network")
}

func register(c echo.Context) error {
	// u := new(NewUser)
	// if err := c.Bind(u); err != nil {
	// 	return c.JSON(http.StatusUnprocessableEntity, map[string]string{
	// 		"error": "invalid json",
	// 	})
	// }
	//
	// if u.Account == "" || u.Password == "" {
	// 	return c.JSON(http.StatusUnprocessableEntity, map[string]string{
	// 		"error": "invalid account details",
	// 	})
	// }
	//
	// db, err := getDB(c)
	// if err != nil {
	// 	return err
	// }
	//
	// var user User
	// if db.First(&user, "account = ?", u.Account).RecordNotFound() {
	// 	hpassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	// 	if err != nil {
	// 		return err
	// 	}
	//
	// 	user = User{
	// 		Rating:   0,
	// 		Trade:    false,
	// 		Account:  u.Account,
	// 		Password: hpassword,
	// 	}
	// 	db.Create(&user)
	//
	// 	return c.JSON(http.StatusOK, user)
	// } else {
	// 	return c.JSON(http.StatusConflict, map[string]string{
	// 		"error": "user exists",
	// 	})
	// }
	return c.JSON(http.StatusOK, "ok")
}

func login(c echo.Context) error {
	type NewUser struct {
		Name     string `json:"name"`
		Password string `json:"password"`
	}
	u := new(NewUser)
	if err := c.Bind(u); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, map[string]string{
			"error": "invalid json",
		})
	}

	if u.Name == "" || u.Password == "" {
		return c.JSON(http.StatusUnprocessableEntity, map[string]string{
			"error": "invalid account details",
		})
	}

	if u.Name == "joy" && u.Password == "123" {
		return c.JSON(http.StatusOK, "ok")
	} else {
		return c.JSON(http.StatusUnauthorized, map[string]string{
			"error": "unauthorized",
		})
	}

	// db, err := getDB(c)
	// if err != nil {
	// 	return err
	// }

	// secret, err := getSecret(c)
	// if err != nil {
	// 	return err
	// }
	//
	// var user User
	// if db.First(&user, "account = ?", u.Account).RecordNotFound() {
	// 	return c.JSON(http.StatusConflict, map[string]string{
	// 		"error": "not found",
	// 	})
	// } else {
	// 	err = bcrypt.CompareHashAndPassword(user.Password, []byte(u.Password))
	// 	if err != nil {
	// 		return c.JSON(http.StatusUnauthorized, map[string]string{
	// 			"error": "unauthorized",
	// 		})
	// 	}
	//
	// 	token := jwt.New(jwt.SigningMethodHS256)
	// 	claims := token.Claims.(jwt.MapClaims)
	// 	claims["account"] = user.Account
	// 	claims["exp"] = time.Now().Add(time.Hour * EXP).Unix()
	//
	// 	t, err := token.SignedString([]byte(secret))
	// 	if err != nil {
	// 		return err
	// 	}
	//
	// 	return c.JSON(http.StatusOK, map[string]string{
	// 		"token": t,
	// 	})
	// }
}

func getDB(c echo.Context) (*gorm.DB, error) {
	db, ok := c.Get("db").(*gorm.DB)
	if !ok {
		return nil, errors.New("no db in context")
	}
	return db, nil
}

func setDB(db *gorm.DB) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("db", db)
			next(c)
			return nil
		}
	}
}

func getSecret(c echo.Context) (string, error) {
	secret, ok := c.Get("secret").(string)
	if !ok {
		return "", errors.New("no secret in context")
	}
	return secret, nil
}

func setSecret(secret string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("secret", secret)
			next(c)
			return nil
		}
	}
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	database_url := os.Getenv("DATABASE_URL")
	if database_url == "" {
		log.Fatal("$DATABASE_URL must be set")
	}

	secret := os.Getenv("SECRET")
	if secret == "" {
		log.Fatal("$SECRET must be set")
	}

	db, err := gorm.Open("postgres", database_url)
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	db.AutoMigrate(&models.User{})
	db.DropTable(&models.User{})
	db.CreateTable(&models.User{})

	// test users for dev ---------------------------------
	testUser := &models.User{Name: "someone", Account: "0x31337"}
	db.Save(testUser)
	// db.Model(testUser).Association("Friends").Append(testFriend1, testFriend2, testFriend3, testFriend4)
	//-----------------------------------------------------

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	// e.Use(setSecret(secret))
	e.Use(setDB(db))

	e.GET("/", home)

	api := e.Group("/api/v1")
	api.POST("/register", register)
	api.POST("/login", login)

	// e.POST("/users", createUser)
	// e.GET("/users/:id", getUser)
	// e.PUT("/users/:id", updateUser)
	// e.DELETE("/users/:id", deleteUser)

	// user := api.Group("/user")
	// user.Use(middleware.JWT([]byte(secret)))
	// user.POST("/match", match)
	// user.POST("/trade", trade)
	// user.POST("/friend/add", add_friend)
	// user.POST("/friend/update", update_friend)
	// user.POST("/friend/delete", delete_friend)
	// user.GET("/friend/list", list_friends)

	e.Logger.Fatal(e.Start(":" + port))
}
