package models

type (
	Organization struct {
		ID   uint   `json:"uuid"`
		Name string `json:"name"`
	}

	Account struct {
		ID              uint   `json:"uuid"`
		Name            string `json:"name"`
		Organization_id uint   `json:"uuid"`
		Customer_id     uint   `json:"uuid"`
	}

	Project struct {
		ID         uint   `json:"uuid"`
		Name       string `json:"name"`
		Account_id uint   `json:"uuid"`
	}

	Task struct {
		ID         uint   `json:"uuid"`
		Name       string `json:"name"`
		Project_id uint   `json:"uuid"`
	}

	Entry struct {
		ID            uint   `json:"uuid"`
		Name          string `json:"name"`
		Task_id       uint   `json:"uuid"`
		Membership_id uint   `json:"uuid"`
		Start_date    uint   `json:"uuid"`
		Stop_date     uint   `json:"uuid"`
	}

	Person struct {
		ID    uint   `json:"uuid"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}

	Membership struct {
		ID         uint   `json:"uuid"`
		Name       string `json:"name"`
		Project_id uint   `json:"uuid"`
		Person_id  uint   `json:"uuid"`
	}

	Rate struct {
		ID            uint   `json:"uuid"`
		Rate          uint   `json:"rate"`
		Membership_id uint   `json:"uuid"`
		Discount      uint   `json:"discount"`
		Basis         string `json:"basis"`
		Currency      string `json:"currency"`
	}

	// Entity struct {
	// 	ID            uint   `json:"uuid"`
	// 	Name          uint   `json:"rate"`
	// 	Membership_id uint   `json:"uuid"`
	// 	Discount      uint   `json:"discount"`
	// 	Basis         string `json:"basis"`
	// 	Currency      string `json:"currency"`
	// }

	Ledger struct {
		ID        uint `json:"uuid"`
		Source_id uint `json:"uuid"`
		Target_id uint `json:"uuid"`
		Amount    uint `json:"amount"`
		Recorded  uint `json:"timestamps"`
	}
)

//------------------------------------

type User struct {
	ID       uint   `json:"-"`
	Name     string `json:"name"`
	Account  string `json:"account"`
	Password []byte `json:"-"`
}

// const (
// 	AliPay Platform = iota
// 	WeChat
// )
//
// type Match struct {
// 	Amount   float64  `json:"amount"`
// 	Price    float64  `json:"price"`
// 	Platform Platform `json:"platform"`
// }
//
// type Trade struct {
// 	Ready    bool     `json:"ready"`
// 	Amount   float64  `json:"amount"`
// 	Price    float64  `json:"price"`
// 	Platform Platform `json:"platform"`
// }
//
// type NewUser struct {
// 	Account  string `json:"account"`
// 	Password string `json:"password"`
// }
//
// type NewFriend struct {
// 	Account string `json:"account"`
// }
//
// type User struct {
// 	ID       uint     `json:"-"`
// 	Rating   uint     `json:"rating"`
// 	Trade    bool     `json:"-"`
// 	Account  string   `json:"account"`
// 	Password []byte   `json:"-"`
// 	Platform Platform `json:"platform"`
// 	Friends  []*User  `json:"-" gorm:"many2many:friends;association_jointable_foreignkey:friend_id"`
// }
