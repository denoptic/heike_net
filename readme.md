### set envs

. env.sh

### heroku

heroku config

heroku config:set SECRET=secret

heroku pg:psql

heroku logs --tail

### sc

abigen -sol contracts/heike.sol -pkg contracts -out contracts/heike.go

go test -v ./contracts/

